import React from "react";
import { hot } from "react-hot-loader";
import Count from "./components/count";
import reactLogo from "../assets/react-logo.png";

class App extends React.Component {
  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    // return null;
    return (
      <div style={{ marginTop: 100 }}>
        <img src={reactLogo} style={{ width: 100 }} /> <br />
        /app1 renders.ss
        <Count />
      </div>
    );
  }
}
export default hot(module)(App);
