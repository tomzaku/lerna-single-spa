import React from "react";

export default () => {
  const [count, setCount] = React.useState(0);
  return (
    <div>
      Work perfectly
      {count}
      <button onClick={() => setCount(count + 1)}>Increasing</button>
      {/* <Button title={"Increasing"} /> */}
    </div>
  );
};
