const {
  loadJs,
  loadCSS,
  loadFile
} = require("../../config/webpack/webpack.module");
const { modulesConfig } = require("../../config/webpack/config");
const appConfig = modulesConfig["app1"];
const merge = require("webpack-merge");

const loadModuleRules = ({ publicPath = "latest" }) => [
  loadJs(),
  loadCSS(),
  loadFile({ publicPath })
];
const buildCommonConfig = config =>
  merge([
    ...loadModuleRules(config),
    {
      entry: "./src/index.js"
    }
  ]);

module.exports = {
  appConfig,
  buildCommonConfig
};
