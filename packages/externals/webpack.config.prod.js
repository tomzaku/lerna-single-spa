const path = require("path");
const merge = require("webpack-merge");
var webpack = require('webpack');

const {
  peerDependenciesEntry,
  peerDependencies,
  getVersionByModule,
  externals
} = require("../../config/webpack/packageMap");
const { rootPath } = require("../../config/webpack/config");
const { loadJs } = require("../../config/webpack/webpack.module");
const loadModuleRules = [loadJs()];
const { root, externals: ex, ...peerDependerciesAppEntry } = peerDependenciesEntry;

module.exports = Object.entries(peerDependerciesAppEntry).map(
  ([moduleName, entry]) => {
    const prodWebpackConfig = {
      entry,
      mode: 'production',
      output: {
        libraryTarget: 'amd',
        path: path.resolve(rootPath, "build", "lib"),
        filename: ({ chunk }) => {
          const version = getVersionByModule(moduleName, chunk.name);
          return `[name]@${version}.js`;
        }
      },
      externals
    };
    return merge([...loadModuleRules, prodWebpackConfig]);
  }
);
