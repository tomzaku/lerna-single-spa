const { buildProductionWebpackConfig } = require("../../config/webpack/helper");
const package = require("./package.json");
const { buildCommonConfig } = require("./webpack.common");

const webpackAppConfig = buildProductionWebpackConfig(
  package,
  buildCommonConfig
);

module.exports = webpackAppConfig;
