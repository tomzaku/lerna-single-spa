const { buildDevWebpackConfig } = require("../../config/webpack/helper");
const package = require("./package.json");
const { appConfig, buildCommonConfig } = require("./webpack.common");

const webpackAppConfig = buildDevWebpackConfig(
  package,
  appConfig,
  buildCommonConfig
);
console.log(webpackAppConfig);
module.exports = webpackAppConfig;
