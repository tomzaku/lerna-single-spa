const { peerDependencies } = require('../../../config/webpack/packageMap')
const fs = require('fs');
const path = require('path');


const buildImportMapJson = () => {
  const packageScope = Object.entries(peerDependencies).reduce((packageMapJson, [packageName, lib]) => {
    const packagePath = `/packages/${packageName}/`
    return {
      ...packageMapJson,
      [packagePath]: lib
    }
  }, {})
  return {
    scopes: {
      "/lib/": {
        "react": "/lib/react@16.8.6.js",
        "react-dom": "/lib/react-dom@16.8.6.js"
      },
      ...packageScope,
    },
  }
}

const savedPath = path.resolve(__dirname, "../public/packagemap.json")

const packagemap = buildImportMapJson()
console.log('BUILD packagemap.json at ', savedPath)
console.log('Contains \n', JSON.stringify(packagemap))
fs.writeFile(
  savedPath,
  JSON.stringify(packagemap),
  function (err) {
    if (err) throw err;
    console.log('complete');
  }
)
