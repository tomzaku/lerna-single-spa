import { registerApplication, start } from "single-spa";
import * as serviceWorker from "./serviceWorker";

const match = prefix => location => {
  return location.hash.startsWith(`#${prefix}`);
};

const registerApp = async ({ name, activeWhen }) => {
  const urlParams = new URLSearchParams(window.location.search);
  const version = urlParams.get(name) || "latest";
  const response = await fetch(`/packages/${name}/${version}/manifest.json`);
  if (response.ok) {
    const { main } = await response.json();
    return registerApplication(
      name,
      async () => {
        const app = await window.System.import(
          `/packages/${name}/${version}/${main}`
        );
        return app;
      },
      activeWhen
    );
  }
};

(function() {
  Promise.all([
    registerApp({ name: "app1", activeWhen: match("/app1") }),
    registerApp({ name: "appTs", activeWhen: match("/appTs") })
  ]);
  start();
})();


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();



