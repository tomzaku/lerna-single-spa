const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const {
  packageConfig,
  modulesConfig,
  buildPath
} = require("../../config/webpack/config");
const merge = require("webpack-merge");

const { loadJs } = require("../../config/webpack/webpack.module");

const appConfig = modulesConfig["root"];

const buildTarget = (host = "localhost", port, hasSsl) => {
  const fullHost = port ? `${host}:${port}` : host;
  return `${(hasSsl && "https") || "http"}://${fullHost}`;
};
const makeProxy = moduleConfig => {
  return Object.keys(moduleConfig).reduce((result, key) => {
    const module = moduleConfig[key];
    const target = buildTarget(
      moduleConfig[key].host,
      moduleConfig[key].port,
      moduleConfig[key].hasSsl
    );
    return {
      ...result,
      [`/packages/${moduleConfig[key].name}`]: {
        target,
        pathRewrite: { [`^/packages/${module.name}`]: "" }
      }
    };
  }, {});
};

const webpackAppConfig = {
  entry: {
    main: "src/index.js",
  },
  output: {
    path: buildPath,
    libraryTarget: "amd",
  },
  resolve: {
    modules: [__dirname, "node_modules"]
  },
  mode: "development",
  plugins: [
    CopyWebpackPlugin([
      { from: path.resolve(__dirname, "public/index.html"), to: "index.html" },
      {
        from: path.resolve(__dirname, "public/packagemap.json"),
        to: "packagemap.json"
      },
      {
        from: path.resolve(__dirname, "public/logo-192x192.png"),
        to: "logo-192x192.png"
      },
      {
        from: path.resolve(__dirname, "public/logo-512x512.png"),
        to: "logo-512x512.png"
      },
      { from: path.resolve(__dirname, "public/style.css"), to: "style.css" },
      {
        from: path.resolve(__dirname, "public/favicon.ico"),
        to: "favicon.ico"
      },
      {
        from: path.resolve(__dirname, "public/manifest.json"),
        to: "manifest.json"
      },
      {
        from: path.resolve(__dirname, "public/service-worker.js"),
        to: "service-worker.js"
      },
      {
        from: "../../node_modules/systemjs/dist/system.min.js",
        to: "lib/system.js"
      },
      {
        from: "../../node_modules/systemjs/dist/extras/amd.min.js",
        to: "lib/amd.js"
      },
      {
        from: "../../node_modules/systemjs/dist/extras/named-exports.min.js",
        to: "lib/named-exports.js"
      }
    ]),
    // new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      { parser: { system: false } },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
        }
      }
    ]
  },
  devtool: 'source-map',
  devServer: {
    historyApiFallback: true,
    port: appConfig.port,
    open: true,
    // Proxy config for development purposes.
    // In production, you would configure you webserver to do something similar.
    proxy: makeProxy(packageConfig),
    disableHostCheck: true
  }
};

const loadModuleRules = [loadJs()];

module.exports = merge([webpackAppConfig]);
