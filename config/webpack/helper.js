const merge = require("webpack-merge");
const path = require("path");
const ManifestPlugin = require("webpack-manifest-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const commonWebpackConfig = require("./webpack.common");
const { moduleRootPath, buildPath, packageBuildPath } = require("./config");
const { externals } = require("./packageMap");

const addSlash = name => (name && `/${name}`) || "";
const makeBuildPath = rootPath => (name, version) =>
  `${rootPath}${addSlash(name)}${addSlash(version)}`;
const buildPublicPath = makeBuildPath(moduleRootPath);
const buildPackageBuildPath = makeBuildPath(packageBuildPath);
const nodeModuleLibPath = makeBuildPath(
  path.resolve(__dirname, "../../node_modules")
);
const buildLibPath = makeBuildPath(
  path.resolve(__dirname, "../../build", "lib")
);

const buildOutput = (publicPath, path, name) => ({
  publicPath: publicPath,
  filename: "main.[hash].js",
  chunkFilename: "[name].[contenthash].js",
  path: path,
  libraryTarget: "amd",
});

const manifest = {
  publicPath: "",
  generate: (seed, files) =>
    files.reduce(
      (manifest, { name, path }) => ({
        ...manifest,
        [name.replace(/(?=\.).*/, "")]: path
      }),
      seed
    )
};

const buildDevWebpackConfig = (
  package,
  moduleConfig,
  buildRestConfig = () => {}
) => {
  const { name } = package;
  const { port } = moduleConfig;
  const version = "latest";
  const publicPath = buildPublicPath(name, version);
  const devWebpackConfig = {
    output: buildOutput(publicPath, buildPackageBuildPath(name, version), name),
    mode: "development",
    devServer: {
      publicPath: `/${version}`,
      port
    },
    plugins: [new ManifestPlugin(manifest)]
  };
  return merge([
    commonWebpackConfig,
    devWebpackConfig,
    buildRestConfig({ version, publicPath })
  ]);
};

const buildProductionWebpackConfig = (package, buildRestConfig = () => {}) => {
  const { name, version } = package;
  const VERSIONS = [version, "latest"];
  return VERSIONS.map(ver => {
    const publicPath = buildPublicPath(name, ver);
    const productionWebpackConfig = {
      output: buildOutput(publicPath, buildPackageBuildPath(name, ver), name),
      plugins: [
        new CleanWebpackPlugin({
          verbose: true
        }),
        new ManifestPlugin(manifest)
      ],
      externals
    };
    return merge([
      commonWebpackConfig,
      productionWebpackConfig,
      buildRestConfig({ version: ver, publicPath })
    ]);
  });
};

module.exports = {
  buildProductionWebpackConfig,
  buildDevWebpackConfig,
  buildOutput,
  makeBuildPath,
  buildPublicPath,
  buildModulePath: buildPackageBuildPath,
  nodeModuleLibPath,
  buildLibPath
};
