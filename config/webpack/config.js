const path = require("path");
const root = require("../../packages/root/package.json");
const externals = require("../../packages/externals/package.json");
const app1 = require("../../packages/app1/package.json");
const appTs = require("../../packages/appTs/package.json");

const rootPath = path.resolve(__dirname, "../..");
const buildPath = path.resolve(rootPath, "./build");
const packageBuildPath = path.resolve(buildPath, "packages");
const moduleRootPath = "/packages";

const packageConfig = {
  [app1.name]: {
    ...app1,
    path: path.resolve(rootPath, "packages/app1"),
    port: 9203
  },
  [appTs.name]: {
    ...appTs,
    path: path.resolve(rootPath, "packages/appTs"),
    port: 9204
  }
};

const modulesConfig = {
  [root.name]: {
    ...root,
    path: path.resolve(rootPath, "packages/root"),
    port: 9200
  },
  [externals.name]: {
    ...externals,
    path: path.resolve(rootPath, "packages/externals"),
    // External will not run in development
    // port: 9201
  },
  ...packageConfig
};

module.exports = {
  modulesConfig,
  moduleRootPath,
  rootPath,
  packageConfig,
  buildPath,
  packageBuildPath
};
