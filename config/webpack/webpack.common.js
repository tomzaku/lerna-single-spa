const path = require("path");
const { externals } = require("./config");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const webpack = require("webpack");

const commonConfig = {
  devServer: {
    watchOptions: { aggregateTimeout: 300, poll: 1000 }
  },
  plugins: []
};
module.exports = commonConfig;
