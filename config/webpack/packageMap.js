const { packageConfig, rootPath } = require("./config");
const path = require("path");

const getLibFilename = (name, version, extension = ".js") =>
  `${name}@${version}${extension}`;

const getLibPathname = (name, libFilename) => `/lib/${libFilename}`;
const getPackageJsonfile = parentPath =>
  path.resolve(parentPath, "package.json");

const getVersion = (moduleNodeModulesLibPath, rootNodeModulesLibPath) => {
  try {
    const { version } = require(getPackageJsonfile(moduleNodeModulesLibPath));
    return version;
  } catch {
    const { version } = require(getPackageJsonfile(rootNodeModulesLibPath));
    return version;
  }
};

const getVersionByModule = (moduleName, libName) => {
  return getVersion(
    path.resolve(packageConfig[moduleName].path, "node_modules", libName),
    path.resolve(rootPath, "node_modules", libName)
  );
};

const peerDependencies = Object.entries(packageConfig).reduce(
  (modulePeer, [appName, appConfig]) => {
    const peerDependenciesPath =
      appConfig.peerDependencies &&
      Object.entries(appConfig.peerDependencies).reduce(
        (peerDeps, [libName]) => {
          const version = getVersionByModule(appName, libName);
          return {
            ...peerDeps,
            [libName]: getLibPathname(libName, getLibFilename(libName, version))
          };
        },
        {}
      );
    return {
      ...modulePeer,
      [appName]: peerDependenciesPath
    };
  },
  {}
);

const getNodeModuleLibPath = (
  moduleNodeModulesLibPath,
  rootNodeModulesLibPath
) => {
  try {
    return require.resolve(moduleNodeModulesLibPath);
  } catch {
    return require.resolve(rootNodeModulesLibPath);
  }
};

const peerDependenciesEntry = Object.entries(packageConfig).reduce(
  (modulePeer, [appName, appConfig]) => {
    const peerDependenciesPath =
      appConfig.peerDependencies &&
      Object.entries(appConfig.peerDependencies).reduce(
        (peerDeps, [packageName]) => {
          const libPath = getNodeModuleLibPath(
            path.resolve(appConfig.path, "node_modules", packageName),
            path.resolve(rootPath, "node_modules", packageName)
          );
          return {
            ...peerDeps,
            [packageName]: libPath
          };
        },
        {}
      );
    return {
      ...modulePeer,
      [appName]: peerDependenciesPath
    };
  },
  {}
);
const externals = Object.values(peerDependencies).reduce((result, lib) => [...result, ...Object.keys(lib)], [])
// const externalLib = peerDependencies
module.exports = {
  externals,
  peerDependenciesEntry,
  peerDependencies,
  getVersionByModule
};
