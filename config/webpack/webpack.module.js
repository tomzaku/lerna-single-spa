const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");

const babelOptions = {
  presets: ["@babel/preset-env", "@babel/preset-react"],
  plugins: [
    "react-hot-loader/babel",
    "ramda",
    "@babel/plugin-proposal-class-properties",
    "@babel/plugin-proposal-object-rest-spread",
    [
      "@babel/plugin-proposal-decorators",
      {
        legacy: true
      }
    ],
    "@babel/plugin-syntax-dynamic-import"
    // "@babel/plugin-transform-runtime"
  ]
};

const loadCSS = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.css$/,
        include,
        exclude,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: true,
              localIdentName: "[path][name]__[local]"
            }
          },
          {
            loader: "postcss-loader",
            options: {
              plugins() {
                return [require("autoprefixer")];
              }
            }
          }
        ]
      }
    ]
  }
});

const loadSCSS = ({ include, exclude, isProduction = false } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(sa|sc)ss$/,
        include,
        exclude,
        use: [
          (isProduction && { loader: MiniCssExtractPlugin.loader }) ||
            "style-loader",
          {
            // Handle import/ require css
            // It help to css for each component
            // https://github.com/webpack-contrib/css-loader#scope
            loader: "css-loader",
            options: {
              sourceMap: true,
              localIdentName: "[path][name]__[local]--[hash:base64:5]",
              camelCase: true,
              // modules: "global"
              modules: true
            }
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true,
              // minimize: true,
              plugins: () => [
                require("postcss-flexbugs-fixes"),
                // Help to generate specific css for each component
                // require('postcss-modules'),
                autoprefixer({
                  browsers: [
                    ">1%",
                    "last 4 versions",
                    "Firefox ESR",
                    "not ie < 9" // React doesn't support IE8 anyway
                  ],
                  flexbox: "no-2009"
                })
              ]
            }
          },
          { loader: "sass-loader", options: { sourceMap: true } }
        ]
      }
    ]
  }
});

const loadTs = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        exclude,
        include,
        use: [
          {
            loader: "babel-loader",
            options: babelOptions
          },
          {
            loader: require.resolve("ts-loader"),
            options: {
              happyPackMode: true,
              // disable type checker - we will use it in fork plugin
              transpileOnly: true
            }
          }
        ]
      }
    ]
  }
});

const loadJs = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        exclude,
        include,
        test: /\.(js|jsx|mjs)$/,
        // use: ['babel-loader', 'source-map-loader'],
        use: [
          {
            loader: "babel-loader",
            options: babelOptions
          }
        ]
      }
    ]
  }
});

const loadFile = ({ include, exclude, publicPath } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(jpg|png|jpeg|gif)$/,
        include,
        exclude,
        use: [
          {
            loader: "file-loader",
            options: {
              // limit: 25000,
              // name: 'static/media/[name].[hash:8].[ext]',
              publicPath: publicPath
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              limit: 50000,
              // name: 'static/media/[name].[hash:8].[ext]',
              publicPath: publicPath
            }
          }
        ]
      }
    ]
  }
});

exports.loadFile = loadFile;
exports.loadTs = loadTs;
exports.loadJs = loadJs;
exports.loadCSS = loadCSS;
exports.loadSCSS = loadSCSS;
